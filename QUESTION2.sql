CREATE TABLE ChanLevels(
Base_level     char(12) PRIMARY KEY NOT NULL,	
all_level      char(12) );

CREATE TABLE CustLevels
(
Store_level   char(12) PRIMARY KEY NOT NULL,    
Retailer_level char(12) NOT NULL);

CREATE TABLE ProdLevels (
Code_level       char(12) PRIMARY KEY NOT NULL,	
Class_level      char(12) NOT NULL,	 
Group_level      char(12) NOT NULL,	
Family_level     char(12) NOT NULL,   
Line_level       char(12) NOT NULL,	  
Division_level   char(12) NOT NULL);
CREATE TABLE TimeLevels(
Tid        varchar(12) PRIMARY KEY NOT NULL,   			
year_level      Number(4)  NOT NULL,			
Quarter_level   varchar(6) NOT NULL, 		 
month_level	Number(2),		
week_level	Number(2),		
day_level       Number(2))

PARTITION BY LIST (Quarter_level)
( PARTITION TIME1 VALUES ('Q1,Q2'),
  PARTITION TIME2 VALUES ('Q3,Q4')
);
		
CREATE TABLE actvarss (
Channel_level       char(12) NOT NULL,
Customer_level      char(12) NOT NULL,
Product_level       char(12) NOT NULL,
Time_level         varchar(12) NOT NULL,
UnitsSold          FLOAT  NOT NULL,
DollarSales         FLOAT  NOT NULL,
DollarCost        FLOAT  NOT NULL,
FOREIGN KEY (Channel_level)  REFERENCES chanlevels(base_level), 
FOREIGN KEY (Customer_level) REFERENCES custlevels(store_level),
FOREIGN KEY (product_level)  REFERENCES prodlevels(code_level),  
FOREIGN KEY (Time_level)  REFERENCES timelevels(tid),
CONSTRAINT fk_actvars_time  FOREIGN KEY(Time_level)  REFERENCES timelevels(tid)
)
PARTITION BY REFERENCE (fk_actvars_time);











